/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.awt.Color;


/**
 *
 * @author AndresFWilT
 */

public class Proceso implements Cloneable {
    
    private char nombre;
    private int tLlegada, tLlegadaAux, rafaga, rafagaEjecutada, tComienzo, tFinal, tRetorno, tEspera, prioridad;
    private Color color;
    
    public Proceso(char nombre, Color color, int tLlegada, int rafaga, int prioridad){
        this.nombre = nombre;
        this.color = color;
        this.tLlegada = tLlegada;
        this.tLlegadaAux = tLlegada;
        this.rafaga = rafaga;
        this.prioridad = prioridad;
        this.rafagaEjecutada = 0;
    }

    public Object clone() throws CloneNotSupportedException  { 
        return super.clone(); 
    } 

    public char getNombre() {
        return nombre;
    }

    public int gettLlegada() {
        return tLlegada;
    }

    public int getRafaga() {
        return rafaga;
    }

    public int gettComienzo() {
        return tComienzo;
    }

    public int gettFinal() {
        return tFinal;
    }

    public int gettRetorno() {
        return tRetorno;
    }

    public int gettEspera() {
        return tEspera;
    }

    public Color getColor() {
        return color;
    }

    public int getPrioridad() {
        return prioridad;
    }

    public int getRafagaEjecutada() {
        return rafagaEjecutada;
    }

    public int getTiempoLlegadaAux() {
        return tLlegadaAux;
    }
    
    public void settComienzo(int tComienzo) {
        this.tComienzo = tComienzo;
    }

    public void settFinal(int tFinal) {
        this.tFinal = tFinal;
    }

    public void settRetorno(int tRetorno) {
        this.tRetorno = tRetorno;
    }

    public void settEspera(int tEspera) {
        this.tEspera = tEspera;
    }

    public void settRafaga(int rafaga) {
        this.rafaga = rafaga;
    }

    public void settRafagaEjecutada(int rafagaEjecutada) {
        this.rafagaEjecutada = rafagaEjecutada;
    }

    public void settLlegadaAux(int tLlegadaAux) {
        this.tLlegadaAux = tLlegadaAux;
    }
}
