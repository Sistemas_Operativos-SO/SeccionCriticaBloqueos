/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.componentes.diagramaGantt;

import java.util.LinkedList;
import logica.Proceso;

/**
 *
 * @author AndresFWilT
 */

public class DiagramaProcesosComponent {
    
    private DiagramaProcesosTemplate diagramaProcesosTemplate;
    private LinkedList<Proceso> procesos;
    
    public DiagramaProcesosComponent() {
        diagramaProcesosTemplate = new DiagramaProcesosTemplate();
    }

    public DiagramaProcesosTemplate gDiagramaProcesosTemplate() {
        return diagramaProcesosTemplate;
    }

    public void addProceso(Proceso proceso){
        diagramaProcesosTemplate.addProDiagrama(proceso);
    }
    
    public void actualizar(){
        diagramaProcesosTemplate.avanceProceso();
    }

    public LinkedList<Proceso> gProcesos(){
        return procesos;
    }
}