/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.componentes.barraHerramientas;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author AndresFWilT
 */


public class BarraHerramientasComponent{

    private BarraHerramientasTemplate barraHerramientasTemplate;

    public BarraHerramientasComponent() {
        barraHerramientasTemplate = new BarraHerramientasTemplate(this);
    }

    public BarraHerramientasTemplate gBarraHerramientasTemplate() {
        return barraHerramientasTemplate;
    }
}