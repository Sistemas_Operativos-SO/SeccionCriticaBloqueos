package view.componentes.barraHerramientas;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import java.awt.Color;
import java.awt.Font;

/**
 *
 * @author AndresFWilT
 */

public class BarraHerramientasTemplate extends JPanel {
    
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private JLabel lTItulo;
    
    //Objetos decoradores
    private Font fuenteTitulo;

    public BarraHerramientasTemplate(BarraHerramientasComponent barraHerramientasComponent) {

        fuenteTitulo = new Font("Impact", Font.PLAIN, 20);
        lTItulo = new JLabel("<html>SECCI&Oacute;N CRITICA BLOQUEOS<html>");
        lTItulo.setFont(fuenteTitulo);
        lTItulo.setBounds(380, 0, 420 , 50);
        lTItulo.setForeground(Color.WHITE);
        lTItulo.setHorizontalAlignment(SwingConstants.CENTER);
        this.add(lTItulo);

        this.setSize(1200, 50);
        this.setLayout(null);
        this.setBackground(new Color(220, 93, 92));
    }
}
