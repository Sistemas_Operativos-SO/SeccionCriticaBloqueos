/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.componentes.tablaProcesos;

import java.awt.Color;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.JLabel;

import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author AndresFWilT
 */
public class TablaProcesosTemplate extends JScrollPane {

    private static final long serialVersionUID = 1L;

    private JTable tabla;
    private DefaultTableModel modeloTabla;

    private Font fuente;
    private Border borderT;

    public TablaProcesosTemplate() {

        fuente = new Font("Impact", Font.PLAIN, 17);
        borderT = BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK);

        tabla = new JTable();
        tabla.setBounds(0, 0, 1200, 275);
        tabla.getTableHeader().setFont(new Font("Impact", Font.PLAIN, 15));
        tabla.setFont(new Font("Impact", Font.PLAIN, 15));
        modeloTabla = new DefaultTableModel();
        modeloTabla.addColumn("Proceso");
        modeloTabla.addColumn("TiempoLlegada");
        modeloTabla.addColumn("Rafaga");
        modeloTabla.addColumn("Tiempo Comienzo");
        modeloTabla.addColumn("Tiempo Final");
        modeloTabla.addColumn("Tiempo Retorno");
        modeloTabla.addColumn("Tiempo Espera");

        tabla.setModel(modeloTabla);

        this.setBorder(BorderFactory.createTitledBorder(borderT, "Tabla de datos", SwingConstants.LEFT, 0, fuente, Color.WHITE));
        this.setViewportView(tabla);
        this.setSize(1200, 275);
        this.setBackground(null);
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        centrarDatos(tabla);
        tabla.setDefaultRenderer(String.class, centerRenderer);
    }

    public void anadirProceso(String[] proceso) {
        modeloTabla.addRow(proceso);
    }

    public void modificarProceso(String[] datosTabla) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        modeloTabla.removeRow(modeloTabla.getRowCount() - 1);
        modeloTabla.addRow(datosTabla);
        centrarDatos(tabla);
        tabla.setDefaultRenderer(String.class, centerRenderer);
    }

    private void centrarDatos(JTable tabla) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        for (int j = 0; j < tabla.getColumnCount(); j++) {
            tabla.getColumnModel().getColumn(j).setCellRenderer(centerRenderer);
        }
    }
}
