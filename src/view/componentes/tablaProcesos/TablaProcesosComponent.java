/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.componentes.tablaProcesos;

/**
 *
 * @author AndresFWilT
 */
public class TablaProcesosComponent {
    
    private TablaProcesosTemplate tablaProcesosTemplate;

    public TablaProcesosComponent(){

        tablaProcesosTemplate = new TablaProcesosTemplate();
    }

    public TablaProcesosTemplate gTablaProcesosTemplate(){
        return tablaProcesosTemplate;
    }

    public void anadirProceso(String[] proceso){
        tablaProcesosTemplate.anadirProceso(proceso);
    }

	public void modificarProceso(String[] datosTabla) {
        tablaProcesosTemplate.modificarProceso(datosTabla);
	}
}
