/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.componentes.panelProcesosEnCola;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import logica.Logica;

/**
 *
 * @author AndresFWilT
 */

public class ProcesosEnColaComponent implements ActionListener {

    private ProcesosEnColaTemplate procesosEnColaTemplate;
    private Logica logica;

    public ProcesosEnColaComponent(Logica logica) {

        this.logica = logica;
        procesosEnColaTemplate = new ProcesosEnColaTemplate(this, logica.getColaProcesosGrafica(), logica.getColaProcesosBloqueados());
    }

    public ProcesosEnColaTemplate gProcesosEnColaTemplate() {
        return procesosEnColaTemplate;
    }

    public void actualizar(){
        procesosEnColaTemplate.actualizar();
    }

    public void actualizarBloqueados(){
        procesosEnColaTemplate.actualizarBloqueados();
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getActionCommand().equals("<html><div align='center'>Agregar Procesos</div></html>")) {
            
            logica.nuevosProcesos();
            procesosEnColaTemplate.actualizar();
        } else if(e.getSource()==procesosEnColaTemplate.getbBloqueados()){
            logica.setBloqueado(true);
        } else if(e.getSource()==procesosEnColaTemplate.getbDesbloquear()){
            logica.desbloquear();
        }
    }
}
